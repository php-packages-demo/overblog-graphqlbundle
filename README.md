# overblog-graphqlbundle

Build a complete GraphQL server in your Symfony App. https://packagist.org/packages/overblog/graphql-bundle

## Unofficial documentation
* (es) [*Symfony y GraphQL: Mutations*](https://medium.com/@ger86/symfony-y-graphql-mutations-880f9f786537)
  2019-06 Gerardo Fernández